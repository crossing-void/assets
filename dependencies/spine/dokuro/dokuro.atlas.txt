
dokuro.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
1113
  rotate: false
  xy: 932, 517
  size: 89, 103
  orig: 89, 103
  offset: 0, 0
  index: -1
221
  rotate: true
  xy: 975, 12
  size: 48, 30
  orig: 48, 30
  offset: 0, 0
  index: -1
2223
  rotate: true
  xy: 911, 310
  size: 128, 110
  orig: 128, 110
  offset: 0, 0
  index: -1
2224
  rotate: true
  xy: 911, 180
  size: 128, 110
  orig: 128, 110
  offset: 0, 0
  index: -1
brow
  rotate: true
  xy: 984, 818
  size: 185, 34
  orig: 185, 34
  offset: 0, 0
  index: -1
cheek_l
  rotate: false
  xy: 932, 476
  size: 72, 39
  orig: 72, 39
  offset: 0, 0
  index: -1
cheek_r
  rotate: false
  xy: 911, 22
  size: 62, 38
  orig: 62, 38
  offset: 0, 0
  index: -1
eye_l
  rotate: true
  xy: 911, 62
  size: 116, 109
  orig: 116, 109
  offset: 0, 0
  index: -1
hair_b
  rotate: true
  xy: 2, 2
  size: 432, 441
  orig: 432, 441
  offset: 0, 0
  index: -1
hair_f
  rotate: true
  xy: 445, 10
  size: 424, 464
  orig: 424, 464
  offset: 0, 0
  index: -1
hair_r
  rotate: true
  xy: 614, 450
  size: 170, 229
  orig: 170, 229
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 614, 622
  size: 381, 368
  orig: 381, 368
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 932, 440
  size: 45, 34
  orig: 45, 34
  offset: 0, 0
  index: -1
ribon_d
  rotate: false
  xy: 2, 436
  size: 610, 567
  orig: 610, 567
  offset: 0, 0
  index: -1
thigh_l
  rotate: false
  xy: 845, 440
  size: 85, 180
  orig: 85, 180
  offset: 0, 0
  index: -1

dokuro2.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
1114
  rotate: false
  xy: 272, 492
  size: 89, 103
  orig: 89, 103
  offset: 0, 0
  index: -1
arm_l
  rotate: false
  xy: 183, 361
  size: 86, 126
  orig: 86, 126
  offset: 0, 0
  index: -1
arm_r
  rotate: false
  xy: 345, 610
  size: 86, 126
  orig: 86, 126
  offset: 0, 0
  index: -1
armupp_l
  rotate: false
  xy: 433, 616
  size: 76, 120
  orig: 76, 120
  offset: 0, 0
  index: -1
armupp_r
  rotate: false
  xy: 511, 616
  size: 76, 120
  orig: 76, 120
  offset: 0, 0
  index: -1
body1
  rotate: true
  xy: 639, 794
  size: 217, 293
  orig: 217, 293
  offset: 0, 0
  index: -1
coat
  rotate: false
  xy: 363, 738
  size: 274, 273
  orig: 274, 273
  offset: 0, 0
  index: -1
eye_r
  rotate: true
  xy: 810, 706
  size: 86, 103
  orig: 86, 103
  offset: 0, 0
  index: -1
hair_l
  rotate: false
  xy: 2, 117
  size: 192, 229
  orig: 192, 229
  offset: 0, 0
  index: -1
hair_r1
  rotate: false
  xy: 183, 489
  size: 87, 277
  orig: 87, 277
  offset: 0, 0
  index: -1
leg_l
  rotate: false
  xy: 272, 597
  size: 71, 169
  orig: 71, 169
  offset: 0, 0
  index: -1
leg_r
  rotate: true
  xy: 639, 721
  size: 71, 169
  orig: 71, 169
  offset: 0, 0
  index: -1
lightring
  rotate: false
  xy: 2, 768
  size: 359, 243
  orig: 359, 243
  offset: 0, 0
  index: -1
ribon_u
  rotate: true
  xy: 2, 348
  size: 418, 179
  orig: 418, 179
  offset: 0, 0
  index: -1
thigh_r
  rotate: false
  xy: 934, 812
  size: 86, 199
  orig: 86, 199
  offset: 0, 0
  index: -1
tie
  rotate: false
  xy: 2, 2
  size: 129, 113
  orig: 129, 113
  offset: 0, 0
  index: -1
