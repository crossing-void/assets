
kuroko.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
221
  rotate: true
  xy: 991, 157
  size: 48, 30
  orig: 48, 30
  offset: 0, 0
  index: -1
2223
  rotate: false
  xy: 885, 207
  size: 128, 110
  orig: 128, 110
  offset: 0, 0
  index: -1
2224
  rotate: false
  xy: 830, 95
  size: 128, 110
  orig: 128, 110
  offset: 0, 0
  index: -1
arm_l
  rotate: false
  xy: 794, 441
  size: 87, 128
  orig: 87, 128
  offset: 0, 0
  index: -1
arm_r
  rotate: true
  xy: 883, 451
  size: 87, 128
  orig: 87, 128
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 403, 129
  size: 215, 302
  orig: 215, 302
  offset: 0, 0
  index: -1
brow
  rotate: true
  xy: 960, 94
  size: 111, 29
  orig: 111, 29
  offset: 0, 0
  index: -1
cheek_l
  rotate: true
  xy: 574, 55
  size: 72, 39
  orig: 72, 39
  offset: 0, 0
  index: -1
cheek_r
  rotate: false
  xy: 812, 230
  size: 62, 38
  orig: 62, 38
  offset: 0, 0
  index: -1
eye_l
  rotate: true
  xy: 885, 319
  size: 130, 110
  orig: 130, 110
  offset: 0, 0
  index: -1
eye_r
  rotate: true
  xy: 830, 2
  size: 91, 104
  orig: 91, 104
  offset: 0, 0
  index: -1
hair1_l
  rotate: true
  xy: 2, 120
  size: 140, 399
  orig: 140, 399
  offset: 0, 0
  index: -1
hair1_r
  rotate: false
  xy: 889, 540
  size: 133, 482
  orig: 133, 482
  offset: 0, 0
  index: -1
hair2_r
  rotate: true
  xy: 416, 571
  size: 144, 427
  orig: 144, 427
  offset: 0, 0
  index: -1
hair3_r
  rotate: true
  xy: 385, 433
  size: 136, 407
  orig: 136, 407
  offset: 0, 0
  index: -1
hair_f
  rotate: false
  xy: 2, 262
  size: 381, 330
  orig: 381, 330
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 594
  size: 412, 428
  orig: 412, 428
  offset: 0, 0
  index: -1
l_hair1
  rotate: true
  xy: 416, 717
  size: 143, 446
  orig: 143, 446
  offset: 0, 0
  index: -1
l_hair2
  rotate: true
  xy: 416, 862
  size: 160, 471
  orig: 160, 471
  offset: 0, 0
  index: -1
l_tie
  rotate: true
  xy: 620, 8
  size: 196, 208
  orig: 196, 208
  offset: 0, 0
  index: -1
leg_l
  rotate: false
  xy: 812, 270
  size: 71, 169
  orig: 71, 169
  offset: 0, 0
  index: -1
leg_r
  rotate: true
  xy: 403, 56
  size: 71, 169
  orig: 71, 169
  offset: 0, 0
  index: -1
mouth
  rotate: true
  xy: 385, 383
  size: 48, 14
  orig: 48, 14
  offset: 0, 0
  index: -1
r_tie
  rotate: false
  xy: 620, 206
  size: 190, 225
  orig: 190, 225
  offset: 0, 0
  index: -1
thigh_l
  rotate: true
  xy: 203, 35
  size: 83, 197
  orig: 83, 197
  offset: 0, 0
  index: -1
thigh_r
  rotate: true
  xy: 2, 32
  size: 86, 199
  orig: 86, 199
  offset: 0, 0
  index: -1

kuroko2.png
size: 512,128
format: RGBA8888
filter: Linear,Linear
repeat: none
1113
  rotate: false
  xy: 170, 19
  size: 89, 103
  orig: 89, 103
  offset: 0, 0
  index: -1
1114
  rotate: false
  xy: 261, 19
  size: 89, 103
  orig: 89, 103
  offset: 0, 0
  index: -1
armupp_l
  rotate: false
  xy: 88, 2
  size: 80, 120
  orig: 80, 120
  offset: 0, 0
  index: -1
armupp_r
  rotate: false
  xy: 2, 2
  size: 84, 120
  orig: 84, 120
  offset: 0, 0
  index: -1
