
satan.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
00
  rotate: false
  xy: 218, 80
  size: 64, 24
  orig: 64, 24
  offset: 0, 0
  index: -1
01
  rotate: false
  xy: 556, 88
  size: 40, 27
  orig: 40, 27
  offset: 0, 0
  index: -1
02
  rotate: false
  xy: 930, 202
  size: 47, 52
  orig: 47, 52
  offset: 0, 0
  index: -1
03
  rotate: true
  xy: 284, 66
  size: 38, 60
  orig: 38, 60
  offset: 0, 0
  index: -1
04
  rotate: false
  xy: 2, 86
  size: 214, 168
  orig: 214, 168
  offset: 0, 0
  index: -1
05
  rotate: false
  xy: 556, 153
  size: 194, 101
  orig: 194, 101
  offset: 0, 0
  index: -1
05a
  rotate: false
  xy: 196, 45
  size: 34, 33
  orig: 34, 33
  offset: 0, 0
  index: -1
06
  rotate: true
  xy: 752, 168
  size: 86, 107
  orig: 86, 107
  offset: 0, 0
  index: -1
1111
  rotate: false
  xy: 984, 162
  size: 36, 39
  orig: 36, 39
  offset: 0, 0
  index: -1
1113
  rotate: true
  xy: 913, 130
  size: 36, 41
  orig: 36, 41
  offset: 0, 0
  index: -1
1114
  rotate: false
  xy: 232, 47
  size: 33, 31
  orig: 33, 31
  offset: 0, 0
  index: -1
13
  rotate: false
  xy: 861, 174
  size: 67, 80
  orig: 67, 80
  offset: 0, 0
  index: -1
15
  rotate: false
  xy: 598, 88
  size: 36, 27
  orig: 36, 27
  offset: 0, 0
  index: -1
16
  rotate: true
  xy: 930, 168
  size: 32, 52
  orig: 32, 52
  offset: 0, 0
  index: -1
20
  rotate: false
  xy: 118, 2
  size: 29, 16
  orig: 29, 16
  offset: 0, 0
  index: -1
21
  rotate: false
  xy: 2, 2
  size: 25, 15
  orig: 25, 15
  offset: 0, 0
  index: -1
22
  rotate: true
  xy: 188, 30
  size: 18, 6
  orig: 18, 6
  offset: 0, 0
  index: -1
220
  rotate: true
  xy: 913, 168
  size: 4, 3
  orig: 4, 3
  offset: 0, 0
  index: -1
221
  rotate: false
  xy: 149, 10
  size: 13, 8
  orig: 13, 8
  offset: 0, 0
  index: -1
2221
  rotate: true
  xy: 979, 203
  size: 51, 40
  orig: 51, 40
  offset: 0, 0
  index: -1
2223
  rotate: false
  xy: 346, 60
  size: 51, 44
  orig: 51, 44
  offset: 0, 0
  index: -1
2224
  rotate: false
  xy: 812, 101
  size: 44, 31
  orig: 44, 31
  offset: 0, 0
  index: -1
23
  rotate: false
  xy: 402, 107
  size: 152, 147
  orig: 152, 147
  offset: 0, 0
  index: -1
24
  rotate: false
  xy: 218, 106
  size: 182, 148
  orig: 182, 148
  offset: 0, 0
  index: -1
26
  rotate: true
  xy: 708, 117
  size: 34, 50
  orig: 34, 50
  offset: 0, 0
  index: -1
27
  rotate: true
  xy: 813, 134
  size: 32, 48
  orig: 32, 48
  offset: 0, 0
  index: -1
30
  rotate: true
  xy: 2, 19
  size: 65, 114
  orig: 65, 114
  offset: 0, 0
  index: -1
31
  rotate: true
  xy: 760, 118
  size: 34, 50
  orig: 34, 50
  offset: 0, 0
  index: -1
32
  rotate: true
  xy: 863, 140
  size: 32, 48
  orig: 32, 48
  offset: 0, 0
  index: -1
34
  rotate: true
  xy: 638, 123
  size: 28, 68
  orig: 28, 68
  offset: 0, 0
  index: -1
35
  rotate: true
  xy: 556, 117
  size: 34, 80
  orig: 34, 80
  offset: 0, 0
  index: -1
37
  rotate: true
  xy: 118, 20
  size: 28, 68
  orig: 28, 68
  offset: 0, 0
  index: -1
38
  rotate: true
  xy: 118, 50
  size: 34, 76
  orig: 34, 76
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 752, 154
  size: 59, 12
  orig: 59, 12
  offset: 0, 0
  index: -1
