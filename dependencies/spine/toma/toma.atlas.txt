
toma.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
1113
  rotate: false
  xy: 699, 221
  size: 89, 103
  orig: 89, 103
  offset: 0, 0
  index: -1
1114
  rotate: false
  xy: 790, 221
  size: 89, 103
  orig: 89, 103
  offset: 0, 0
  index: -1
221
  rotate: false
  xy: 864, 342
  size: 48, 30
  orig: 48, 30
  offset: 0, 0
  index: -1
2223
  rotate: true
  xy: 587, 216
  size: 128, 110
  orig: 128, 110
  offset: 0, 0
  index: -1
2224
  rotate: false
  xy: 587, 104
  size: 128, 110
  orig: 128, 110
  offset: 0, 0
  index: -1
arm_l
  rotate: true
  xy: 650, 482
  size: 87, 128
  orig: 87, 128
  offset: 0, 0
  index: -1
arm_r
  rotate: true
  xy: 780, 482
  size: 87, 128
  orig: 87, 128
  offset: 0, 0
  index: -1
armupp_l
  rotate: true
  xy: 742, 404
  size: 76, 120
  orig: 76, 120
  offset: 0, 0
  index: -1
armupp_r
  rotate: true
  xy: 742, 326
  size: 76, 120
  orig: 76, 120
  offset: 0, 0
  index: -1
body1
  rotate: false
  xy: 2, 43
  size: 230, 289
  orig: 230, 289
  offset: 0, 0
  index: -1
brow
  rotate: false
  xy: 650, 571
  size: 261, 39
  orig: 261, 39
  offset: 0, 0
  index: -1
cheek_l
  rotate: false
  xy: 2, 2
  size: 72, 39
  orig: 72, 39
  offset: 0, 0
  index: -1
cheek_r
  rotate: false
  xy: 913, 572
  size: 62, 38
  orig: 62, 38
  offset: 0, 0
  index: -1
eye_l
  rotate: false
  xy: 630, 370
  size: 110, 110
  orig: 110, 110
  offset: 0, 0
  index: -1
eye_r
  rotate: false
  xy: 864, 374
  size: 79, 106
  orig: 79, 106
  offset: 0, 0
  index: -1
hair_b
  rotate: false
  xy: 2, 334
  size: 538, 211
  orig: 538, 211
  offset: 0, 0
  index: -1
hair_f
  rotate: false
  xy: 2, 547
  size: 646, 446
  orig: 646, 446
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 650, 612
  size: 381, 368
  orig: 381, 368
  offset: 0, 0
  index: -1
leg_l
  rotate: false
  xy: 441, 163
  size: 71, 169
  orig: 71, 169
  offset: 0, 0
  index: -1
leg_r
  rotate: false
  xy: 514, 163
  size: 71, 169
  orig: 71, 169
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 234, 49
  size: 36, 7
  orig: 36, 7
  offset: 0, 0
  index: -1
skirt_b
  rotate: false
  xy: 234, 145
  size: 205, 187
  orig: 205, 187
  offset: 0, 0
  index: -1
thigh_l
  rotate: true
  xy: 234, 58
  size: 85, 194
  orig: 85, 194
  offset: 0, 0
  index: -1
thigh_r
  rotate: false
  xy: 542, 346
  size: 86, 199
  orig: 86, 199
  offset: 0, 0
  index: -1
