
iriya.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
1113
  rotate: true
  xy: 912, 12
  size: 89, 103
  orig: 89, 103
  offset: 0, 0
  index: -1
221
  rotate: true
  xy: 2, 7
  size: 48, 30
  orig: 48, 30
  offset: 0, 0
  index: -1
2223
  rotate: true
  xy: 512, 13
  size: 128, 110
  orig: 128, 110
  offset: 0, 0
  index: -1
2224
  rotate: true
  xy: 624, 19
  size: 128, 110
  orig: 128, 110
  offset: 0, 0
  index: -1
arm_l
  rotate: false
  xy: 729, 186
  size: 86, 126
  orig: 86, 126
  offset: 0, 0
  index: -1
arm_r
  rotate: false
  xy: 736, 58
  size: 86, 126
  orig: 86, 126
  offset: 0, 0
  index: -1
armupp_l
  rotate: true
  xy: 390, 6
  size: 81, 120
  orig: 81, 120
  offset: 0, 0
  index: -1
armupp_r
  rotate: false
  xy: 817, 192
  size: 81, 120
  orig: 81, 120
  offset: 0, 0
  index: -1
body1
  rotate: true
  xy: 2, 57
  size: 212, 298
  orig: 212, 298
  offset: 0, 0
  index: -1
brow
  rotate: false
  xy: 34, 8
  size: 193, 47
  orig: 193, 47
  offset: 0, 0
  index: -1
cheek_l
  rotate: false
  xy: 229, 16
  size: 72, 39
  orig: 72, 39
  offset: 0, 0
  index: -1
cheek_r
  rotate: true
  xy: 303, 6
  size: 62, 38
  orig: 62, 38
  offset: 0, 0
  index: -1
collar
  rotate: false
  xy: 900, 103
  size: 110, 102
  orig: 110, 102
  offset: 0, 0
  index: -1
eye_l
  rotate: false
  xy: 900, 207
  size: 116, 105
  orig: 116, 105
  offset: 0, 0
  index: -1
eye_r
  rotate: false
  xy: 824, 2
  size: 86, 99
  orig: 86, 99
  offset: 0, 0
  index: -1
hair_b
  rotate: false
  xy: 2, 271
  size: 435, 751
  orig: 435, 751
  offset: 0, 0
  index: -1
hair_f
  rotate: false
  xy: 439, 314
  size: 516, 708
  orig: 516, 708
  offset: 0, 0
  index: -1
hair_u
  rotate: true
  xy: 623, 149
  size: 163, 104
  orig: 163, 104
  offset: 0, 0
  index: -1
leg_l
  rotate: false
  xy: 477, 143
  size: 71, 169
  orig: 71, 169
  offset: 0, 0
  index: -1
leg_r
  rotate: false
  xy: 550, 143
  size: 71, 169
  orig: 71, 169
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 229, 5
  size: 13, 9
  orig: 13, 9
  offset: 0, 0
  index: -1
thigh_l
  rotate: false
  xy: 390, 89
  size: 85, 180
  orig: 85, 180
  offset: 0, 0
  index: -1
thigh_r
  rotate: false
  xy: 302, 70
  size: 86, 199
  orig: 86, 199
  offset: 0, 0
  index: -1

iriya2.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
1114
  rotate: false
  xy: 2, 2
  size: 89, 103
  orig: 89, 103
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 107
  size: 381, 368
  orig: 381, 368
  offset: 0, 0
  index: -1
