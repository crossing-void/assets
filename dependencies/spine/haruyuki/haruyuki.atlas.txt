
haruyuki.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
1113
  rotate: false
  xy: 756, 480
  size: 81, 144
  orig: 81, 144
  offset: 0, 0
  index: -1
1114
  rotate: false
  xy: 640, 474
  size: 114, 150
  orig: 114, 150
  offset: 0, 0
  index: -1
2223
  rotate: false
  xy: 939, 428
  size: 78, 102
  orig: 78, 102
  offset: 0, 0
  index: -1
2224
  rotate: false
  xy: 640, 237
  size: 104, 107
  orig: 104, 107
  offset: 0, 0
  index: -1
arm_l
  rotate: false
  xy: 640, 346
  size: 101, 126
  orig: 101, 126
  offset: 0, 0
  index: -1
arm_r
  rotate: false
  xy: 839, 552
  size: 101, 126
  orig: 101, 126
  offset: 0, 0
  index: -1
armupp_l
  rotate: false
  xy: 839, 437
  size: 98, 113
  orig: 98, 113
  offset: 0, 0
  index: -1
armupp_r
  rotate: false
  xy: 407, 141
  size: 98, 113
  orig: 98, 113
  offset: 0, 0
  index: -1
body1
  rotate: true
  xy: 384, 256
  size: 316, 254
  orig: 316, 254
  offset: 0, 0
  index: -1
eye_l
  rotate: false
  xy: 297, 45
  size: 108, 152
  orig: 108, 152
  offset: 0, 0
  index: -1
eye_r
  rotate: false
  xy: 942, 532
  size: 78, 146
  orig: 78, 146
  offset: 0, 0
  index: -1
hair_b
  rotate: true
  xy: 436, 626
  size: 347, 400
  orig: 347, 400
  offset: 0, 0
  index: -1
hair_f
  rotate: false
  xy: 2, 574
  size: 432, 399
  orig: 432, 399
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 199
  size: 380, 373
  orig: 380, 373
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 2, 2
  size: 26, 10
  orig: 26, 10
  offset: 0, 0
  index: -1
thigh_l
  rotate: false
  xy: 838, 680
  size: 183, 293
  orig: 183, 293
  offset: 0, 0
  index: -1
thigh_r
  rotate: true
  xy: 2, 14
  size: 183, 293
  orig: 183, 293
  offset: 0, 0
  index: -1
