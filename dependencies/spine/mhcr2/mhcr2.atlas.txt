
mhcr2.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 828, 67
  size: 67, 90
  orig: 67, 90
  offset: 0, 0
  index: -1
10
  rotate: false
  xy: 828, 19
  size: 47, 46
  orig: 47, 46
  offset: 0, 0
  index: -1
11
  rotate: false
  xy: 743, 162
  size: 128, 80
  orig: 128, 80
  offset: 0, 0
  index: -1
12
  rotate: true
  xy: 373, 4
  size: 28, 31
  orig: 28, 31
  offset: 0, 0
  index: -1
13
  rotate: false
  xy: 966, 27
  size: 54, 64
  orig: 54, 64
  offset: 0, 0
  index: -1
14
  rotate: true
  xy: 293, 2
  size: 30, 45
  orig: 30, 45
  offset: 0, 0
  index: -1
15
  rotate: false
  xy: 897, 67
  size: 67, 90
  orig: 67, 90
  offset: 0, 0
  index: -1
16
  rotate: true
  xy: 873, 159
  size: 83, 96
  orig: 83, 96
  offset: 0, 0
  index: -1
17
  rotate: true
  xy: 743, 15
  size: 47, 80
  orig: 47, 80
  offset: 0, 0
  index: -1
19
  rotate: false
  xy: 134, 5
  size: 110, 27
  orig: 110, 27
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 743, 64
  size: 83, 96
  orig: 83, 96
  offset: 0, 0
  index: -1
20
  rotate: true
  xy: 521, 34
  size: 208, 220
  orig: 208, 220
  offset: 0, 0
  index: -1
3
  rotate: false
  xy: 971, 162
  size: 47, 80
  orig: 47, 80
  offset: 0, 0
  index: -1
4
  rotate: false
  xy: 2, 34
  size: 295, 208
  orig: 295, 208
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 2, 18
  size: 130, 14
  orig: 130, 14
  offset: 0, 0
  index: -1
6
  rotate: true
  xy: 299, 34
  size: 208, 220
  orig: 208, 220
  offset: 0, 0
  index: -1
7
  rotate: true
  xy: 340, 4
  size: 28, 31
  orig: 28, 31
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 966, 93
  size: 54, 64
  orig: 54, 64
  offset: 0, 0
  index: -1
9
  rotate: true
  xy: 246, 2
  size: 30, 45
  orig: 30, 45
  offset: 0, 0
  index: -1
